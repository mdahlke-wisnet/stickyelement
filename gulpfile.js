const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');


//Concat and minify JS
gulp.task('scripts', function () {
	return gulp.src('src/*.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(sourcemaps.init())
		.pipe(concat('sticky-element.js'))
		.pipe(gulp.dest('build'))
		.pipe(rename('sticky-element.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('build'));
});

//Compile Scss and minify output
gulp.task('styles', function () {
	gulp.src('src/css/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(rename('sticky-element.css'))
		.pipe(sourcemaps.write())
		.pipe(cleanCSS())
		.pipe(rename('sticky-element.min.css'))
		.pipe(gulp.dest('./build/'));
});

//Watch task
gulp.task('watch', function () {
	gulp.watch('src/css/*.scss', ['styles']);
	gulp.watch('src/*.js', ['scripts']);
});

gulp.task('default', ['scripts', 'styles', 'watch']);
