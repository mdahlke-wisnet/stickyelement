;
(function ($, window) {

	$.fn.stickyelement = function (options) {
		return this.each(function (i, e) {
			return new $se(e, options);
		});
	};

	var defaults = {
		subject: null,
		target: null,
		placeholder: false,
		keepContents: false,
		offset: 0,
		stickyClass: 'sticky-element-fixed',
		minWidth: null,
		beforeInit: function () {
			return null;
		},
		onStick: function () {
			return null;
		},
		onUnStick: function () {
			return null;
		},
		onResize: function () {
			return null;
		}
	};


	$.stickyelement = function (el, options) {
		var self = this;
		var scrolled;

		this.settings = $.extend(true, {}, defaults, options);
		this.subject = this.settings.subject === null ? $(el) : this.settings.subject;
		this.target = this.settings.target === null ? $(el) : this.settings.target;
		this.isFixed = false;

		this.setup = function () {
			self.init();
		};
		this.resize = function () {
			self.settings.onResize(self);
		};
		this.scroll = function () {
			self.shouldIStick();
		}

		this.setup();
	};

	var $se = $.stickyelement;

	$se.fn = $se.prototype = {
		stickyelement: '0.0.3'
	};

	$se.fn.extend = $se.extend = $.extend;

	$se.fn.extend({
		init: function () {
			var self = this;
			this.settings.beforeInit(this);
			this.subjectTop = this.subject.offset().top;
			$(window).on('resize', this.resize);
			$(window).on('scroll', this.scroll);
			this.shouldIStick();
		},
		shouldIStick: function(){
			var self = this;
			if( self.settings.minWidth == null || $(window).width() > this.settings.minWidth ) {
				scrolled = $(window).scrollTop() + self.settings.offset;
				if (!self.isFixed && scrolled > self.subjectTop) {
					self.isFixed = true;
					self.settings.onStick(self);
					self.target.addClass(self.settings.stickyClass);
				}
				else if (self.isFixed && scrolled <= self.subjectTop) {
					self.isFixed = false;
					self.settings.onUnStick(self);
					self.target.removeClass(self.settings.stickyClass);
				}
			}
		},
		reload: function() {
			this.target.removeClass(this.settings.stickyClass);
			this.init();
		}
	});

})(jQuery, window);