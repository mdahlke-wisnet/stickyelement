'use strict';

;
(function ($, window) {

	$.fn.stickyelement = function (options) {
		return this.each(function (i, e) {
			return new $se(e, options);
		});
	};

	var defaults = {
		subject: null,
		target: null,
		placeholder: false,
		keepContents: false,
		offset: 0,
		stickyClass: 'sticky-element-fixed',
		minWidth: null,
		beforeInit: function beforeInit() {
			return null;
		},
		onStick: function onStick() {
			return null;
		},
		onUnStick: function onUnStick() {
			return null;
		},
		onResize: function onResize() {
			return null;
		}
	};

	$.stickyelement = function (el, options) {
		var self = this;
		var scrolled;

		this.settings = $.extend(true, {}, defaults, options);
		this.subject = this.settings.subject === null ? $(el) : this.settings.subject;
		this.target = this.settings.target === null ? $(el) : this.settings.target;
		this.isFixed = false;

		this.setup = function () {
			self.init();
		};
		this.resize = function () {
			self.settings.onResize(self);
		};
		this.scroll = function () {
			self.shouldIStick();
		};

		this.setup();
	};

	var $se = $.stickyelement;

	$se.fn = $se.prototype = {
		stickyelement: '0.0.3'
	};

	$se.fn.extend = $se.extend = $.extend;

	$se.fn.extend({
		init: function init() {
			var self = this;
			this.settings.beforeInit(this);
			this.subjectTop = this.subject.offset().top;
			$(window).on('resize', this.resize);
			$(window).on('scroll', this.scroll);
			this.shouldIStick();
		},
		shouldIStick: function shouldIStick() {
			var self = this;
			if (self.settings.minWidth == null || $(window).width() > this.settings.minWidth) {
				scrolled = $(window).scrollTop() + self.settings.offset;
				if (!self.isFixed && scrolled > self.subjectTop) {
					self.isFixed = true;
					self.settings.onStick(self);
					self.target.addClass(self.settings.stickyClass);
				} else if (self.isFixed && scrolled <= self.subjectTop) {
					self.isFixed = false;
					self.settings.onUnStick(self);
					self.target.removeClass(self.settings.stickyClass);
				}
			}
		},
		reload: function reload() {
			this.target.removeClass(this.settings.stickyClass);
			this.init();
		}
	});
})(jQuery, window);
"use strict";

!function (t, s) {
  t.fn.stickyelement = function (t) {
    return this.each(function (s, e) {
      return new i(e, t);
    });
  };var e = { subject: null, target: null, placeholder: !1, keepContents: !1, offset: 0, stickyClass: "sticky-element-fixed", minWidth: null, beforeInit: function beforeInit() {
      return null;
    }, onStick: function onStick() {
      return null;
    }, onUnStick: function onUnStick() {
      return null;
    }, onResize: function onResize() {
      return null;
    } };t.stickyelement = function (s, i) {
    var n = this;this.settings = t.extend(!0, {}, e, i), this.subject = null === this.settings.subject ? t(s) : this.settings.subject, this.target = null === this.settings.target ? t(s) : this.settings.target, this.isFixed = !1, this.setup = function () {
      n.init();
    }, this.resize = function () {
      n.settings.onResize(n);
    }, this.scroll = function () {
      n.shouldIStick();
    }, this.setup();
  };var i = t.stickyelement;i.fn = i.prototype = { stickyelement: "0.0.3" }, i.fn.extend = i.extend = t.extend, i.fn.extend({ init: function init() {
      this.settings.beforeInit(this), this.subjectTop = this.subject.offset().top, t(s).on("resize", this.resize), t(s).on("scroll", this.scroll), this.shouldIStick();
    }, shouldIStick: function shouldIStick() {
      var e = this;(null == e.settings.minWidth || t(s).width() > this.settings.minWidth) && (scrolled = t(s).scrollTop() + e.settings.offset, !e.isFixed && scrolled > e.subjectTop ? (e.isFixed = !0, e.settings.onStick(e), e.target.addClass(e.settings.stickyClass)) : e.isFixed && scrolled <= e.subjectTop && (e.isFixed = !1, e.settings.onUnStick(e), e.target.removeClass(e.settings.stickyClass)));
    }, reload: function reload() {
      this.target.removeClass(this.settings.stickyClass), this.init();
    } });
}(jQuery, window);